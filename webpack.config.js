const path = require("path");
const webpack = require("webpack");

module.exports = {
    mode: "development",
    watch: true,
    entry: "./src/index.tsx",
    devServer: {
        contentBase: './public',
    },  
    plugins: [new webpack.HotModuleReplacementPlugin()],
    output: {
        path: path.resolve(__dirname, "dist/"),
        publicPath: "/dist/",
        filename: "bundle.js"
    },
    resolve: {
        extensions: [".ts", ".tsx", ".js", ".json"]
    },
    devtool: "source-map",
    module: {
        rules: [
            { test: /\.scss$/, use: [ "style-loader", "css-loader", "sass-loader" ] },
            { test: /\.tsx?$/, loader: "babel-loader" },
            { test: /\.tsx?$/, loader: "ts-loader" },
            { enforce: "pre", test: /\.js$/, loader: "source-map-loader" },
            { test: /\.(png|svg|jpg|gif)$/, use: ["file-loader"] },
            { test: /\.(json)$/, use: ["file-loader"] },
        ]
    }
};