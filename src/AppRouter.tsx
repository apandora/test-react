import * as React from "react";
import {
    BrowserRouter as Router,
    Switch,
    Route,
} from "react-router-dom";

import List from './pages/List';
import Details from './pages/Details';

const AppRouter = () => {

    return (
        <Router>
            <Switch>
                <Route exact path="/" component={List} />
                <Route exact path="/:slug" component={Details} />
            </Switch>
        </Router>
    );

};

export default AppRouter;