import * as React from "react";
import clsx from "clsx";
import { useState, useEffect } from "react";
import { Grid, Box } from "@material-ui/core";

import { API } from "../API";
import PageLayout from "../../components/PageLayout";
import { IDetails } from "./IDetails";
import Picture from "../../components/Picture";

import "./style.scss";

const Details = (props: IDetails.IProps): JSX.Element => {

    const [data, setData] = useState<any>(null);

    useEffect(() => {
        API.getList().then(response => {
            let d = response.find((item: any) => {
                return item.slug === props.match.params.slug;
            });

            setData(d);
        })
    }, [props.match.params.slug]);

    if(data){
        const {image, tag, title, questions} = data;

        let questionItems = questions.map((question: string, index: number) => {
            return (
                <>
                    <h3 className={clsx("dtlSubtitle")}>Question {(index+1)}</h3>
                    <p className={clsx("dtlQuestion")}>{question}</p>
                </>
            );
        });

        return (
            <PageLayout>
                <Grid className={clsx("detailpage")} container justify="center" spacing={2}>
                    <Grid item xs={12} md={6}>
                        <Picture src={image} tag={tag} />
                    </Grid>
                    <Grid item xs={12} md={6}>
                        <Box className={clsx("detailcontent")} display="flex" flexDirection="column">
                            <h1 className={clsx("dtlTitle")}>{title}</h1>
                            {questionItems}
                        </Box>
                    </Grid>
                </Grid>
            </PageLayout>
        );
    }
    else{
        return (
            <PageLayout>
                <p>No data.</p>
            </PageLayout>
        );
    }

};

export default Details;