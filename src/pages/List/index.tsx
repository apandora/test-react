import * as React from "react";
import { useState, useEffect } from "react";
import { Grid } from "@material-ui/core";

import { API } from "../API";
import PageLayout from "../../components/PageLayout";
import ItemCard from "../../components/ItemCard";

const List = () => {

    const [dataList, setDataList] = useState<any[]>([]);

    useEffect(() => {
        API.getList().then(response => {
            setDataList(response);
        })
    }, []);
    
    if(dataList.length > 0){
        let itemcards = dataList.map((itemData, i) => {
            return (
                <Grid key={i} item xs={12} md={6} style={{margin: "1rem 0rem"}}>
                    <ItemCard itemData={itemData} />
                </Grid>
            );
        });

        return (
            <PageLayout>
                <Grid container justify="center" spacing={2}>
                    { itemcards }
                </Grid>
            </PageLayout>
        );
    }
    else{
        return (
            <PageLayout>
                <p>No data.</p>
            </PageLayout>
        );
    }
};

export default List;