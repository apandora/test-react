export class API {
    public static getList() {
        return fetch("/data.json", {
            method: 'get',
            headers: { 'Content-Type': 'application/json' },
        })
        .then(response => response.json());
    }
}