import "core-js";
import "regenerator-runtime/runtime";
import * as React from "react";
import * as ReactDOM from "react-dom";
import "./styles/global.scss";
import AppRouter from "./AppRouter";

ReactDOM.render(
    <AppRouter />,
    document.getElementById("app"),
);