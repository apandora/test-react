import * as React from "react";
import clsx from "clsx";
import Box from '@material-ui/core/Box';

import LinkItem from "../LinkItem";
import "./style.scss";

const Header = () => {

    return (
        <Box className={clsx("header")} display="flex" flexDirection="row" justifyContent="space-between" alignItems="center">
            <a href="/">
                <img className={clsx("hdrLogo")} src="/assets/adrenalin_black.svg" alt="Logo" />
            </a>
            <Box display="flex" flexDirection="row" alignItems="center">
                <LinkItem title="Culture" href="#" />
                <LinkItem title="Work" href="#" />
                <LinkItem title="Clients" href="#" />
                <LinkItem title="Services" href="#" />
                <LinkItem title="Careers" href="#" />
                <LinkItem title="Contact" href="#" />
            </Box>
        </Box>
    );

};

export default Header;