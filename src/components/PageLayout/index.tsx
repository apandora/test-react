import * as React from "react";

import Header from "../Header";
import Footer from "../Footer";

import { IPageLayout } from "./IPageLayout";
import './style.scss';

const PageLayout = (props: IPageLayout.IProps): JSX.Element => {

    return (
        <div className="page-layout">
            <Header />
            <main>
                {props.children}
            </main>
            <Footer />
        </div>
    );

};

export default PageLayout;