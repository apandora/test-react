import { ReactNode, Props } from "react";

export namespace IPageLayout {
    export interface IProps extends Props<{}> {
        children?: ReactNode;
    }
}
