import * as React from "react";
import clsx from "clsx";
import Box from '@material-ui/core/Box';

import LinkItem from "../LinkItem";
import "./style.scss";

const Footer = () => {

    return (
        <Box className={clsx("footer")} display="flex" flexDirection="row" justifyContent="space-between" alignItems="center">
            <img className={clsx("ftrLogo")} src="/assets/adrenalin_black.svg" alt="Logo" />
            <Box display="flex" flexDirection="row" alignItems="center">
                <LinkItem title="Privacy" href="#" spacing={2.25} />
                <LinkItem title="Sitemap" href="#" spacing={2.25} />
                <LinkItem title="Facebook" href="#" spacing={2.25} />
                <LinkItem title="LinkedIn" href="#" spacing={2.25} />
                <LinkItem title="Instagram" href="#" spacing={2.25} />
                <LinkItem title="Twitter" href="#" spacing={2.25} />
            </Box>
        </Box>
    );

};

export default Footer;