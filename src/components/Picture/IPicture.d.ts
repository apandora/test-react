import { Props } from "react";

export namespace IPicture {
    export interface IProps extends Props<{}> {
        src: string;
        tag: string;
    }
}
