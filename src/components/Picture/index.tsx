import * as React from "react";
import clsx from "clsx";

import "./style.scss";
import { IPicture } from "./IPicture";

const Picture = (props: IPicture.IProps): JSX.Element => {

    const {src, tag} = props;

    return (
        <div style={{position: "relative"}}>
            <img className={clsx("picture-img")} src={"/images/"+src} alt={src} />
            <span className={clsx("picture-tag")}>{tag}</span>
        </div>
    );

};

export default Picture;