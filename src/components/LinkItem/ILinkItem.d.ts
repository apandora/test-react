import { Props } from "react";

export namespace ILinkItem {
    export interface IProps extends Props<{}> {
        href: string;
        title: string;
        spacing?: number;
    }
}
