import * as React from "react";
import { Link } from "react-router-dom";
import clsx from "clsx";

import { ILinkItem } from "./ILinkItem";
import './style.scss';

const PageLayout = (props: ILinkItem.IProps): JSX.Element => {

    return (
        <Link 
            style={{marginLeft: (props.spacing ? props.spacing : 2.5)+"rem"}} 
            className={clsx("linkitem")} 
            to={props.href}
        >
            {props.title}
        </Link>
    );

};

export default PageLayout;