import { Props } from "react";

export namespace IItemCard {
    export interface IProps extends Props<{}> {
        itemData: any;
    }
}
