import * as React from "react";
import clsx from "clsx";
import { Box } from "@material-ui/core";
import { Link } from "react-router-dom";

import "./style.scss";
import { IItemCard } from "./IItemCard";
import Picture from "../Picture";

const ItemCard = (props: IItemCard.IProps): JSX.Element => {

    const {slug, title_long, thumb, tag} = props.itemData;

    return (
        <Box className={clsx("itemcard")} display="flex" flexDirection="column">
            <Picture src={thumb} tag={tag} />
            <h4 className={clsx("itemcard-title")}>{title_long}</h4>
            <Link 
                className={clsx("itemcard-link")} 
                to={"/"+slug}
            >
                &#x2015;&nbsp;&nbsp;&nbsp;View Case Study
            </Link>
        </Box>
    );

};

export default ItemCard;